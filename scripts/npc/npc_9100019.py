# Character field ID when accessed: 910000000
# ObjectID: 1000003
# ParentID: 9100019
# Object Position Y: -344
# Object Position X: -747
# ARI in free market, a money loaner. But only when in need.

sm.sendNext("Hello Traveler, if you are in need of money I can give you some. But only if you are in actual need.")
sm.sendNext("If you have less then 20m, I will give you 20m. Cheers!")

chrMesos = sm.getMesos()

if chrMesos < 20000000:
    sm.giveMesos(20000000)
    sm.sendSayOkay("You got your mesos man, get lost now...")

else:
    sm.sendSayOkay("I am not a charity dude, get lost.")