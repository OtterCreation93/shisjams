# Character field ID when accessed: 910000000
# ObjectID: 1000002
# ParentID: 9100018
# Object Position X: -652
# Object Position Y: -335
# SHIRO in Free market (will give free levels)

buying = sm.sendAskYesNo("Hello mapler, do you want some speedboosting to test stuff?")

if buying:
    try:
        numberOfLevels = sm.sendAskNumber("How many levels do you need?", 0, 0, 99)
        if numberOfLevels > 0:
            curLevel = chr.getLevel()
            sm.levelUntil(curLevel+numberOfLevels)
        else:
            sm.sendSayOkay('Ok. Bye.')
    except:
        sm.sendSayOkay("Okay! Bye bye!")

else:
    sm.sendSayOkay("Okay! Bye bye!")