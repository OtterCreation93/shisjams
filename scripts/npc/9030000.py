# ParentID: 9030000
# Character field ID when accessed: 910000000
# ObjectID: 1000000
# Object Position Y: -184
# Object Position X: -857
# Free market shop for cubes

buying = sm.sendAskYesNo("Hello mapler, do you want to buy some cubes?")

if buying:
    try:
        numberOfCubes = sm.sendAskNumber("How many black cubes do you want?", 0, 0, 99)
        if numberOfCubes > 0:
            sm.giveItem(5062010, numberOfCubes)
        else:
            sm.sendSayOkay('Ok. Bye.')
    except:
        sm.sendSayOkay('Something went wrong. Try again later.')
